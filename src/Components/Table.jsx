import React from 'react';
import Button from './Button';
import './Table.css'

const Table = ({ items, sortItems, deleteItem }) => {
  const renderHeaders = () => {
    const headers = Object.keys(items[0])
    
    // Convert keys from camel case to title case
    const convertToTitleCase = (string) => {
      return string.replace(/([A-Z])/g, ' $1')
        .replace(/^./, str => str.toUpperCase())
    }
    
    return headers.map((key, index) => {
      return (
        <th key={index}>
          <div className="table-headers">
            {convertToTitleCase(key)}

            <div className="sort-buttons">
              <Button
                type="button"
                className="sort-button"
                onClick={() => sortItems(key, 'ascending')}
              >
                <i className="fas fa-caret-square-up"></i>
              </Button>

              <Button
                type="button"
                className="sort-button"
                onClick={() => sortItems(key, 'descending')}
              >
                <i className="fas fa-caret-square-down"></i>
              </Button>
            </div>
          </div>
        </th>
      )
    });
  };

  const renderItems = items.map((item, index) => {
    const fields = Object.keys(items[0])
    
    return (
      <tr key={index}>
        {fields.map((field) => {
          return <td key={item[field]}>{item[field]}</td>
        })}
        <td>
          <Button
            type="button"
            className="delete-button"
            onClick={() => {
              alert('Are you sure you want to delete this?')
              deleteItem(index)
              }
            }
          >
            <i className="fas fa-times"></i>
          </Button>
        </td>
      </tr>
    )
  });

  return (
    <div className="table-container">
      <table className="table">
        <thead>
          <tr>
            {renderHeaders()}
            <th></th>
          </tr>
        </thead>
        <tbody>
          {renderItems}
        </tbody>
      </table>
    </div>
  )
};

export default Table;