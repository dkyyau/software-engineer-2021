import React from 'react';

const InputField = ({ name, type, label, value, onChange, required, pattern, title }) => {
  return (
    <label htmlFor={name} className="form-label">
      {label}
      <input 
        name={name}
        type={type}
        value={value}
        placeholder={label}
        onChange={onChange}
        className="form-input"
        required={required}
        pattern={pattern}
        title={title}
      />
    </label>
  )
};

export default InputField;