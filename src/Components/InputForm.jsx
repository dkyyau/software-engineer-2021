import React, { useState } from 'react';
import InputField from './InputField';
import Button from './Button';
import './InputForm.css'

const InputForm = ({ onFormSubmit }) => {
  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [emailAddress, setEmailAddress] = useState('')

  const handleSubmit = event => {
    event.preventDefault();
    
    const newEmployee = {
      firstName: firstName,
      lastName: lastName,
      emailAddress: emailAddress,
    }

    // Clear input fields after submit
    setFirstName('');
    setLastName('');
    setEmailAddress('');

    onFormSubmit(newEmployee);
  }

  return (
    <div className="form-container">
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <InputField
            type="text"
            name="firstName"
            label="First name"
            value={firstName}
            onChange={e => setFirstName(e.target.value)}
            required="required"
            pattern="^(?=.{2,30}$)[A-Za-z]+((\\s|\\-|\\')[A-Za-z]+)?$"
            title="Must be a valid first name."
          />
        </div>
        <div className="form-group">
          <InputField
            type="text"
            name="lastName"
            label="Last name"
            value={lastName}
            onChange={e => setLastName(e.target.value)}
            required="required"
            pattern="^(?=.{2,30}$)[A-Za-z]+((\\s|\\-|\\')[A-Za-z]+)?$"
            title="Must be a valid last name."
          />
        </div>
        <div className="form-group">
          <InputField
            type="email"
            name="emailAddress"
            label="Email address"
            value={emailAddress}
            onChange={e => setEmailAddress(e.target.value)}
            required="required"
          />
        </div>
        <Button type="submit" className="submit-button">Add employee</Button>
      </form>
    </div>
  )
};

export default InputForm;