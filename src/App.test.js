import { render, screen } from '@testing-library/react';
import App from './App';

describe('App', () => {
  test('renders App component', () => {
    render(<App />);
  });

  test('render Employees title', () => {
    render(<App />);

    const title = screen.getByText('Employees');
    expect(title).toBeInTheDocument();
  });
});

describe('Table', () => {
  test('renders Table component', () => {
    render(<App />);

    const table = document.querySelector('.table');
    expect(table).toBeInTheDocument();
  });

  test('renders table headers', () => {
    render(<App />);

    const header1 = screen.getByText('First Name');
    expect(header1).toBeInTheDocument();
    const header2 = screen.getByText('Last Name');
    expect(header2).toBeInTheDocument();
    const header3 = screen.getByText('Email Address');
    expect(header3).toBeInTheDocument();
  });
});
