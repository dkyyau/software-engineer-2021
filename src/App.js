import React, { useState } from 'react';
import Header from './Components/Header';
import InputForm from './Components/InputForm';
import Table from './Components/Table';
import './App.css';

const defaultEmployees = [
  {
    firstName: 'Jane',
    lastName: 'Doe',
    emailAddress: 'jane@companyname.com',
  },
  {
    firstName: 'John',
    lastName: 'Smith',
    emailAddress: 'john@companyname.com',
  },
  {
    firstName: 'David',
    lastName: 'Brown',
    emailAddress: 'david@companyname.com',
  },
  {
    firstName: 'Amy',
    lastName: 'Johnson',
    emailAddress: 'amy@companyname.com',
  },
];

const App = () => {
  const [employees, setEmployees] = useState(defaultEmployees);

  const addEmployee = newEmployee => {
    setEmployees(employees => [...employees, newEmployee]);
  };

  const deleteEmployee = index => {
    let updatedEmployees = [...employees];

    updatedEmployees.splice(index, 1);

    setEmployees(updatedEmployees);
  };

  const sortEmployees = (field, direction) => {
    let sortedEmployees = [...employees];

    sortedEmployees.sort((a, b) => {
      if (a[field] < b[field]) {
        return direction === 'ascending' ? -1 : 1;
      }
      if (a[field] > b[field]) {
        return direction === 'ascending' ? 1 : -1;
      }
      return 0;
    });

    setEmployees(sortedEmployees);
  };

  return (
    <div className="container">
      <Header>Employees</Header>
      <Table
        items={employees}
        sortItems={sortEmployees}
        deleteItem={deleteEmployee}
      />
      <InputForm onFormSubmit={addEmployee} />
    </div>
  );
};

export default App;
